
# Simple Vector Database Project

This project demonstrates how to create a simple in-memory vector database using Rust. The database supports basic operations such as adding vectors, querying the nearest neighbor based on Euclidean distance, aggregating an average vector from all stored vectors, and visualizing the average vector.


## Usage

The main functionality of this project includes data ingestion, querying for the nearest neighbor, calculating and aggregating an average vector, and visualizing this average vector.

### Data Ingestion

Vectors can be added to the database using the `add_vector` method. Each vector should have a unique ID and the data as a list of `f64`.

### Querying

The `nearest_neighbor` method allows querying the database for the vector closest to a given query vector, based on Euclidean distance.

### Aggregating

The `average_vector` method computes an average vector from all vectors stored in the database.

### Visualization

The `visualize_vector` function generates a plot of a given vector (intended to be used with the average vector) and saves it as an image file. This requires the `plotters` library.

## Example

Below is an example that showcases the whole process, included in the `main` function of the project:

```rust
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut db = VectorDB::new();

    // Data Ingestion
    db.add_vector(Vector { id: 1, data: vec![1.0, 2.0, 3.0] });
    db.add_vector(Vector { id: 2, data: vec![4.0, 5.0, 6.0] });
    db.add_vector(Vector { id: 3, data: vec![7.0, 8.0, 9.0] });

    // Nearest Neighbor Query
    let query_vector = Vector { id: 4, data: vec![5.0, 5.0, 5.0] };
    if let Some(nearest) = db.nearest_neighbor(&query_vector) {
        println!("The nearest neighbor is Vector with id: {}", nearest.id);
    }

    // Aggregating Average Vector
    if let Some(avg_vector) = db.average_vector() {
        println!("Average Vector: {:?}", avg_vector.data);

        // Visualization
        visualize_vector(&avg_vector, "average_vector.png")?;
    }

    Ok(())
}
```

Run the project using Cargo:

```sh
cargo run
```

## Result
![image](d476a424904ede06d9cfbf1accd35a7.png)
![image](average_vector.png)
